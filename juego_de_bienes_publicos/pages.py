from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants


class Instrucciones(Page):

    def is_displayed(self):
        return self.round_number == 1
    

class Contribucion(Page):
    form_model = 'player'
    form_fields = ['contribucion']

    def vars_for_template(self):
        if self.round_number <= Constants.periodos_extra_tiempo:
            return {
                'tiempo_de_espera': Constants.tiempo_decision + Constants.tiempo_extra_periodos_decision,
            }
        else:
            return {
                'tiempo_de_espera': Constants.tiempo_decision,
            }

class EsperaResultados(WaitPage):

    def after_all_players_arrive(self):
        self.group.calcular_pagos()

class Resultados(Page):
    timeout_seconds = Constants.tiempo_vista

    def vars_for_template(self):
        return {
            'ingreso_por_fichas_retenidas': Constants.dotacion - self.player.contribucion,
            'tiempo_de_espera': Constants.tiempo_vista,
        }

class ResultadosGrupo(Page):

    

    def get_timeout_seconds (self):
        if self.round_number <= Constants.periodos_extra_tiempo:
            return Constants.tiempo_vista + Constants.tiempo_extra_periodos_informacion
        else:
            return Constants.tiempo_vista

    def vars_for_template(self):
        tiempo_de_espera = 0
        porcentaje_de_jugadores = []

        if self.round_number <= Constants.periodos_extra_tiempo:
            tiempo_de_espera= Constants.tiempo_vista + Constants.tiempo_extra_periodos_informacion
        else:
            tiempo_de_espera = Constants.tiempo_vista

        for p in self.player.get_others_in_group():
            porcentaje_de_jugadores.append( p.contribucion * 100 / Constants.dotacion )
        
        return {
            'porcentaje_de_jugador': self.player.contribucion * 100 / Constants.dotacion,
            'tiempo_de_espera': tiempo_de_espera,
            'porcentaje_de_otros_jugadores': porcentaje_de_jugadores,
        }
            
        

page_sequence = [
    Contribucion,
    EsperaResultados,
    Resultados,
    ResultadosGrupo
]
