from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)


author = ' '

doc = """

"""


class Constants(BaseConstants):
    name_in_url = 'juego_de_bienes_publicos'
    players_per_group = 4
    num_rounds = 10

    dotacion = c(20)
    multiplicador = 1.6
    cantidad_por_participar = c(20)
    tiempo_decision = 60
    tiempo_vista = 30
    periodos_extra_tiempo = 2
    tiempo_extra_periodos_decision = 30
    tiempo_extra_periodos_informacion = 15


class Subsession(BaseSubsession):
    pass
 
class Group(BaseGroup):
    contribucion_total = models.CurrencyField()
    ingreso_individual = models.CurrencyField()

    def calcular_pagos(self):
        self.contribucion_total =   sum([p.contribucion for p in self.get_players()])
        self.ingreso_individual =   self.contribucion_total * Constants.multiplicador / Constants.players_per_group
        for p in self.get_players():
            p.payoff = (Constants.dotacion - p.contribucion) + self.ingreso_individual


class Player(BasePlayer):
    contribucion =                      models.CurrencyField(min=0, max=Constants.dotacion)
